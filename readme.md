# Honcho API package

This package provides a "vanilla" way of communicating with Honcho. This package has been tested on PHP 5.3+.

## Installation

Either:

* Clone the repository
* Download the file directly from repository

## How to use

    include '{directory}/Honcho.php';
    $honcho = new \TTD\HonchoApi\Honcho;
    $honcho->setCredentials($key, $secret); # alternative: new \TTD\HonchoApi\Honcho($key, $secret);
    $get = $honcho->get(); // gets new datasets
    $honcho->post(array $dataset, string $type, string $provider); // creates a new dataset