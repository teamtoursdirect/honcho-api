<?php

class HonchoTest extends \Tests\TestCase
{
    /**
     * @var \TTD\HonchoApi\Honcho
     */
    protected $honcho;

    public function setUp()
    {
        parent::setUp();
        $this->honcho = new \TTD\HonchoApi\Honcho;
    }

    public function test_construct_sets_credentials()
    {
        $this->assertEquals('', $this->honcho->getKey());
        $this->assertEquals('', $this->honcho->getSecret());

        $honcho = new \TTD\HonchoApi\Honcho('12', '34');
        $this->assertEquals('12', $honcho->getKey());
        $this->assertEquals('34', $honcho->getSecret());
    }

    public function test_set_credentials()
    {
        $this->honcho->setCredentials('12', '34');
        $this->assertEquals('12', $this->honcho->getKey());
        $this->assertEquals('34', $this->honcho->getSecret());
    }

    public function test_set_url()
    {
        $this->honcho->setUrl('aaa');
        $this->assertEquals('aaa', $this->honcho->getUrl());
    }

    public function test_get_fail()
    {
        $mock = Mockery::mock('TTD\HonchoAPI\Honcho[request]');
        $mock->setUrl('aaa');
        $mock->shouldReceive('request')->once()->with('aaa/')->andReturn(null);

        try {
            $mock->get();
        } catch (Exception $e) {
            $this->assertEquals('Invalid JSON returned from the API.', $e->getMessage());
        }
    }

    public function test_get()
    {
        $mock = Mockery::mock('TTD\HonchoAPI\Honcho[request]');
        $mock->setUrl('aaa');
        $mock->shouldReceive('request')->once()->with('aaa/')->andReturn(json_decode(json_encode([
            'data' => ['datasets' => ['a']]
        ])));

        $this->assertEquals(['a'], $mock->get());
    }

    public function test_post_fail()
    {
        $mock = Mockery::mock('TTD\HonchoAPI\Honcho[request]');
        try {
            $mock->post([], [], []);
        } catch (Exception $e) {
            $this->assertEquals('Provided $type was not a string.', $e->getMessage());
        }

        try {
            $mock->post([], 'aaa', []);
        } catch (Exception $e) {
            $this->assertEquals('Provided $provider was not a string.', $e->getMessage());
        }

        $mock->setUrl('aaa');
        $mock->shouldReceive('request')->once()->with('aaa/', [
            'data' => json_encode(['a' => 'b']),
            'type' => 'aa',
            'provider' => 'bb'
        ])->andReturn(null);

        try {
            $mock->post(['a' => 'b'], 'aa', 'bb');
        } catch (Exception $e) {
            $this->assertEquals('Invalid JSON returned from the API.', $e->getMessage());
        }
    }

    public function test_post()
    {
        $mock = Mockery::mock('TTD\HonchoAPI\Honcho[request]');
        $mock->setUrl('aaa');
        $mock->shouldReceive('request')->once()->with('aaa/', [
            'data' => json_encode(['a' => 'b']),
            'type' => 'aa',
            'provider' => 'bb'
        ])->andReturn(json_decode(json_encode(['data' => ['stored' => true]])));

        $this->assertTrue($mock->post(['a' => 'b'], 'aa', 'bb'));
    }

    public function test_request_bad_http_code()
    {
        $mock = Mockery::mock('TTD\HonchoAPI\Honcho[curlInit,setCurlOptions,curlExecute,curlGetInfo,curlClose]')
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('curlInit')->once()->andReturn('resource');

        $mock->shouldReceive('setCurlOptions')->once()->andReturn($mock);
        $mock->shouldReceive('curlExecute')->once()->andReturn(true);
        $mock->shouldReceive('curlGetInfo')->once()->andReturn(['http_code' => 0]);
        $mock->shouldReceive('curlClose')->once();

        try {
            $mock->request('aaa');
        } catch (Exception $e) {
            $this->assertEquals("Invalid response from API. Code: 0\nResult: true", $e->getMessage());
        }
    }

    public function test_request_options()
    {
        $mock = Mockery::mock('TTD\HonchoAPI\Honcho[curlInit,setCurlOptions,curlExecute,curlGetInfo,curlClose,defaultCurlOptions]')
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('curlInit')->once()->andReturn('resource');
        $mock->shouldReceive('defaultCurlOptions')->once()->andReturn([1000 => 'true']);

        $mock->shouldReceive('setCurlOptions')->once()->with('resource', [
            1000 => 'true',
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => ['a' => 'b'],
            2000 => 'false'
        ])->andReturn($mock);

        $mock->shouldReceive('curlExecute')->once()->andReturn(json_encode(['cool']));
        $mock->shouldReceive('curlGetInfo')->once()->andReturn(['http_code' => 200]);
        $mock->shouldReceive('curlClose')->once();

        $this->assertEquals(['cool'], $mock->request('aaa', ['a' => 'b'], [2000 => 'false']));
    }

    /**
     * @group failing
     * @expectedException \TTD\HonchoApi\ValidationException
     */
    public function test_request_handle_validation()
    {
        $mock = Mockery::mock('TTD\HonchoAPI\Honcho[curlInit,setCurlOptions,curlExecute,curlGetInfo,curlClose]')
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('curlInit')->once()->andReturn('resource');

        $mock->shouldReceive('setCurlOptions')->once()->andReturn($mock);
        $mock->shouldReceive('curlExecute')->once()->andReturn(json_encode(['validation' => ['a' => ['a is invalid']]]));
        $mock->shouldReceive('curlGetInfo')->once()->andReturn(['http_code' => 422]);
        $mock->shouldReceive('curlClose')->once();

        $mock->request(['a' => 'b']);
    }

    public function test_request()
    {
        $mock = Mockery::mock('TTD\HonchoAPI\Honcho[curlInit,setCurlOptions,curlExecute,curlGetInfo,curlClose]')
            ->shouldAllowMockingProtectedMethods();

        $mock->shouldReceive('curlInit')->once()->andReturn('resource');

        $mock->shouldReceive('setCurlOptions')->once()->andReturn($mock);
        $mock->shouldReceive('curlExecute')->once()->andReturn(json_encode(['cool']));
        $mock->shouldReceive('curlGetInfo')->once()->andReturn(['http_code' => 200]);
        $mock->shouldReceive('curlClose')->once();

        $this->assertEquals(['cool'], $mock->request('aaa'));
    }

    public function test_handle_error()
    {
        try {
            $this->honcho->handleCurlError(401, true);
        } catch (Exception $e) {
            $this->assertEquals('Invalid credentials', $e->getMessage());
        }

        try {
            $this->honcho->handleCurlError(0, true);
        } catch (Exception $e) {
            $this->assertEquals("Invalid response from API. Code: 0\nResult: true", $e->getMessage());
        }
    }
}
