<?php

namespace TTD\HonchoApi;

use \Exception;

/**
 * This is the "vanilla" way of communicating with Honcho - no vendor dependencies.
 * The only dependency is CURL, PHP and PHP-JSON.
 *
 * This is built with legacy in mind, so it'll work with PHP 5.3+. It may even work with lower version, but that
 * hasn't been tested.
 *
 */
class Honcho
{
    /**
     * Honcho constructor.
     *
     * @param string $key
     * @param string $secret
     */
    public function __construct($key = '', $secret = '')
    {
        $this->setCredentials($key, $secret);
    }

    /**
     * The API key
     *
     * @var string
     */
    protected $key;

    /**
     * The API secret
     *
     * @var string
     */
    protected $secret;

    /**
     * The Honcho URL
     *
     * @var string
     */
    protected $url = 'https://honcho.sertxcrm.com';

    /**
     * @param string $key
     * @param string $secret
     * @return $this
     */
    public function setCredentials($key, $secret)
    {
        return $this->setKey($key)->setSecret($secret);
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * Set the API key
     *
     * @param string $key
     * @return $this
     */
    public function setKey($key)
    {
        return $this->setString('key', $key);
    }

    /**
     * Set the API secret
     *
     * @param string $secret
     * @return $this
     */
    public function setSecret($secret)
    {
        return $this->setString('secret', $secret);
    }

    /**
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        return $this->setString('url', $url);
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get all of the datasets that the key has not pulled yet.
     *
     * @return \stdClass
     * @throws \Exception
     */
    public function get()
    {
        $result = $this->request($this->buildUrl());

        if (isset($result->data, $result->data->datasets)) {
            return $result->data->datasets;
        }

        throw new Exception("Invalid JSON returned from the API.");
    }

    /**
     * Create a new dataset
     *
     * @param array  $data
     * @param string $type
     * @param string $provider
     * @return \stdClass
     * @throws \Exception
     */
    public function post(array $data, $type, $provider)
    {
        if (!is_string($type)) {
            throw new Exception('Provided $type was not a string.');
        }

        if (!is_string($provider)) {
            throw new Exception('Provided $provider was not a string.');
        }

        $postData = [
            'data' => json_encode($data),
            'type' => $type,
            'provider' => $provider
        ];

        $result = $this->request($this->buildUrl(), $postData);

        if (isset($result->data, $result->data->stored)) {
            return $result->data->stored;
        }

        throw new Exception('Invalid JSON returned from the API.');
    }

    /**
     * Do either a get/post request depending on whether there is post fields
     *
     * @param string $url
     * @param array  $postData
     * @param array  $extraOptions
     * @return \stdClass
     */
    public function request($url, array $postData = [], array $extraOptions = [])
    {
        $ch = $this->curlInit();

        $options = $this->defaultCurlOptions($url);

        // set the request to a post request
        if ($postData) {
            $options = array_replace($options, [
                CURLOPT_POST => count($postData),
                CURLOPT_POSTFIELDS => $postData
            ]);
        }

        // merge in extra options. Merge this in last so we can override any option with $extraOptions
        $options = array_replace($options, $extraOptions);

        // set the curl options and execute
        $result = $this->setCurlOptions($ch, $options)->curlExecute($ch);
        $info = $this->curlGetInfo($ch);
        $this->curlClose($ch);

        if ($info['http_code'] !== 200) {
            // this will *always* throw an exception
            $this->handleCurlError($info['http_code'], $result);
        }

        return json_decode($result);
    }

    /**
     * Handle the CURL error
     *
     * @param int   $httpCode
     * @param mixed $result
     * @throws \Exception
     */
    public function handleCurlError($httpCode, $result)
    {
        switch ($httpCode) {
            case 401:
                throw new Exception("Invalid credentials");
            case 422:
                throw new ValidationException(json_decode($result)->validation);
        }
        $result = json_encode($result);
        throw new Exception("Invalid response from API. Code: {$httpCode}\nResult: {$result}");
    }

    /**
     * @param string $url
     * @return array
     */
    protected function defaultCurlOptions($url)
    {
        return [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => [
                'api-key: ' . $this->key,
                'api-secret: ' . $this->secret,
            ]
        ];
    }

    /**
     * @param resource $resource
     * @return $this
     */
    protected function curlClose($resource)
    {
        curl_close($resource);
        return $this;
    }

    /**
     * @param resource $resource
     * @return string|array
     */
    protected function curlGetInfo($resource)
    {
        return curl_getinfo($resource);
    }

    /**
     * @param resource $resource
     * @return mixed
     */
    protected function curlExecute($resource)
    {
        return curl_exec($resource);
    }

    /**
     * @param resource $resource
     * @param array    $options
     * @return $this
     */
    protected function setCurlOptions($resource, array $options)
    {
        curl_setopt_array($resource, $options);
        return $this;
    }

    /**
     * @return resource
     */
    protected function curlInit()
    {
        return curl_init();
    }

    /**
     * Set a string value on this class
     *
     * @param string $variable
     * @param string $value
     * @return $this
     * @throws \Exception
     */
    protected function setString($variable, $value)
    {
        if (!is_string($value)) {
            throw new Exception("Provided {$variable} is not a string.");
        }
        $this->$variable = $value;
        return $this;
    }

    /**
     * @param string $path
     * @return string
     */
    protected function buildUrl($path = '')
    {
        return rtrim($this->url, '/') . '/' . ltrim($path, '/');
    }
}

/**
 * Handy exception that you can catch in your code.
 *
 * @package TTD\HonchoApi
 */
class ValidationException extends Exception
{
    public $errors = [];

    public function __construct($errors)
    {
        $this->errors = $errors;
    }
}
